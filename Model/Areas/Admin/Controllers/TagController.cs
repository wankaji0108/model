﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Model.Models; 
using Model.Repositories; 
using System.Threading.Tasks;

[Area("Admin")]
[Authorize(Roles = SD.Role_Admin)]
public class TagController : Controller
{
   
    private readonly ITagRepository _tagRepository;

    public TagController(ITagRepository tagRepository)
    {
        _tagRepository = tagRepository;
    }

    // Hiển thị danh sách Tag
    public async Task<IActionResult> Index()
    {
        var tags = await _tagRepository.GetAllAsync();
        return View(tags);
    }

    // Hiển thị form thêm Tag mới
    public IActionResult Add()
    {
        return View();
    }

    // Xử lý thêm Tag mới
    [HttpPost]
    public async Task<IActionResult> Add(Tag tag)
    {
        if (ModelState.IsValid)
        {
            await _tagRepository.AddAsync(tag);
            return RedirectToAction(nameof(Index));
        }
        // Nếu ModelState không hợp lệ, hiển thị form với dữ liệu đã nhập
        return View(tag);
    }

    // Hiển thị thông tin chi tiết Tag
    public async Task<IActionResult> Display(string id)
    {
        var tag = await _tagRepository.GetByIdAsync(id);
        if (tag == null)
        {
            return NotFound();
        }
        return View(tag);
    }

    // Hiển thị form cập nhật Tag
    public async Task<IActionResult> Update(string id)
    {
        var tag = await _tagRepository.GetByIdAsync(id);
        if (tag == null)
        {
            return NotFound();
        }
        return View(tag);
    }

    // Xử lý cập nhật Tag
    [HttpPost]
    public async Task<IActionResult> Update(string id, Tag tag)
    {
        if (id != tag.TagID)
        {
            return NotFound();
        }
        if (ModelState.IsValid)
        {
            await _tagRepository.UpdateAsync(tag);
            return RedirectToAction(nameof(Index));
        }
        return View(tag);
    }

    // Hiển thị form xác nhận xóa Tag
    public async Task<IActionResult> Delete(string id)
    {
        var tag = await _tagRepository.GetByIdAsync(id);
        if (tag == null)
        {
            return NotFound();
        }
        return View(tag);
    }

    // Xử lý xóa Tag
    [HttpPost, ActionName("Delete")]
    public async Task<IActionResult> DeleteConfirmed(string id)
    {
        await _tagRepository.DeleteAsync(id);
        return RedirectToAction(nameof(Index));
    }
}
