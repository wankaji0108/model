﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Model.Models;
using Model.Repositories;

namespace Model.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = SD.Role_Admin)]
    public class UserController : Controller
    {
        private readonly IUserRepository _userRepository;

        public UserController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        // Display a list of users
        public async Task<IActionResult> Index()
        {
            var users = await _userRepository.GetAllAsync();
            return View(users);
        }

        // Display a form to add a new user
        public async Task<IActionResult> Add()
        {
            // You can manually create a list of roles here if needed
            ViewBag.Roles = new SelectList(new[] { "Role1", "Role2", "Role3" }, "RoleID", "RoleName");
            return View();
        }

        // Handle adding a new user
        [HttpPost]
        public async Task<IActionResult> Add(User user)
        {
            if (ModelState.IsValid)
            {
                await _userRepository.AddAsync(user);
                return RedirectToAction(nameof(Index));
            }
            // Manually create a list of roles here if needed
            ViewBag.Roles = new SelectList(new[] { "Role1", "Role2", "Role3" }, "RoleID", "RoleName");
            return View(user);
        }

        // Display user details
        public async Task<IActionResult> Display(string id)
        {
            var user = await _userRepository.GetByIdAsync(id);
            if (user == null)
            {
                return NotFound();
            }
            return View(user);
        }

        // Display a form to update a user
        public async Task<IActionResult> Update(string id)
        {
            var user = await _userRepository.GetByIdAsync(id);
            if (user == null)
            {
                return NotFound();
            }
            // Manually create a list of roles here if needed
            ViewBag.Roles = new SelectList(new[] { "Role1", "Role2", "Role3" }, "RoleID", "RoleName", user.RoleID);

            return View(user);
        }

        // Handle updating a user
        [HttpPost]
        public async Task<IActionResult> Update(string id, User user)
        {
            if (id!= user.UserID)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                await _userRepository.UpdateAsync(user);
                return RedirectToAction(nameof(Index));
            }
            // Manually create a list of roles here if needed
            ViewBag.Roles = new SelectList(new[] { "Role1", "Role2", "Role3" }, "RoleID", "RoleName", user.RoleID);

            return View(user);
        }

        // Display a form to confirm deleting a user
        public async Task<IActionResult> Delete(string id)
        {
            var user = await _userRepository.GetByIdAsync(id);
            if (user == null)
            {
                return NotFound();
            }
            return View(user);
        }

        // Handle deleting a user
        [HttpPost, ActionName("Delete")]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            await _userRepository.DeleteAsync(id);
            return RedirectToAction(nameof(Index));
        }
    }
}