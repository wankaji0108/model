﻿using Microsoft.AspNetCore.Mvc;

namespace Model.Areas.Admin.Controllers
{
    [Route("admin/home")]
    public class HomeController : Controller
    {
        [Route("")]
        public IActionResult Index()
        {
            return View();
        }
    }
}
