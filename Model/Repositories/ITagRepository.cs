﻿using Model.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Model.Repositories
{
    public interface ITagRepository
    {
        Task<IEnumerable<Tag>> GetAllAsync();
        Task<Tag> GetByIdAsync(string id);
        Task AddAsync(Tag tag);
        Task UpdateAsync(Tag tag);
        Task DeleteAsync(string id);
    }
}