﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Model.Data;
using Model.Models;
using Model.Repositories;
using Model.Models; // Thay thế bằng namespace thực tế của bạn

public class EFTagRepository : ITagRepository
{
    private readonly ApplicationDbContext _context;

    public EFTagRepository(ApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<IEnumerable<Tag>> GetAllAsync()
    {
        return await _context.Tags.ToListAsync();
    }

    public async Task<Tag> GetByIdAsync(string id)
    {
        return await _context.Tags.FindAsync(id);
    }

    public async Task AddAsync(Tag tag)
    {
        _context.Tags.Add(tag);
        await _context.SaveChangesAsync();
    }

    public async Task UpdateAsync(Tag tag)
    {
        _context.Tags.Update(tag);
        await _context.SaveChangesAsync();
    }

    public async Task DeleteAsync(string id)
    {
        var tag = await _context.Tags.FindAsync(id);
        _context.Tags.Remove(tag);
        await _context.SaveChangesAsync();
    }
}