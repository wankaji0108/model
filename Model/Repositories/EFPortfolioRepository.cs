﻿using Microsoft.EntityFrameworkCore;
using Model.Data;
using Model.Models;

namespace Model.Repositories
{
    public class EFPortfolioRepository : IPortfolioRepository
    {
        private readonly ApplicationDbContext _context;

        public EFPortfolioRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Portfolio>> GetAllAsync()
        {
            return await _context.Portfolios.ToListAsync();
        }

        public async Task<Portfolio> GetByIdAsync(int id)
        {
            return await _context.Portfolios.FindAsync(id);
        }

        public async Task AddAsync(Portfolio portfolio)
        {
            _context.Portfolios.Add(portfolio);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(Portfolio portfolio)
        {
            _context.Portfolios.Update(portfolio);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            var portfolio = await _context.Portfolios.FindAsync(id);
            _context.Portfolios.Remove(portfolio);
            await _context.SaveChangesAsync();
        }
    }
}
