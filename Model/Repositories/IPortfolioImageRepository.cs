﻿using Model.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Model.Repositories
{
    public interface IPortfolioImageRepository
    {
        Task<IEnumerable<PortfolioImage>> GetAllAsync();
        Task<PortfolioImage> GetByIdAsync(int id);
        Task AddAsync(PortfolioImage portfolioImage);
        Task UpdateAsync(PortfolioImage portfolioImage);
        Task DeleteAsync(int id);
    }
}