﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Model.Models;

namespace Model.Data
{
    public class ApplicationDbContext : IdentityDbContext<IdentityUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Role> Roles { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Guest> Guests { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Portfolio> Portfolios { get; set; }
        public DbSet<PortfolioImage> PortfolioImages { get; set; }
        public DbSet<Comment> Comments { get; set; }



        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder); // Gọi phương thức OnModelCreating của lớp cha

            // Thiết lập các quan hệ nối và ràng buộc ngoại khoá ở đây
            modelBuilder.Entity<User>()
                .HasOne(u => u.Role)
                .WithMany()
                .HasForeignKey(u => u.RoleID);

            modelBuilder.Entity<Account>()
                .HasOne(a => a.User)
                .WithMany()
                .HasForeignKey(a => a.UserID);

            modelBuilder.Entity<Employee>()
                .HasOne(e => e.Account)
                .WithOne()
                .HasForeignKey<Employee>(e => e.AccountID);

            modelBuilder.Entity<Guest>()
                .HasOne(g => g.Account)
                .WithOne()
                .HasForeignKey<Guest>(g => g.AccountID);

            modelBuilder.Entity<Portfolio>()
                .HasOne(p => p.Guest)
                .WithMany()
                .HasForeignKey(p => p.AccountID);

            modelBuilder.Entity<Portfolio>()
                .HasOne(p => p.Tag)
                .WithMany()
                .HasForeignKey(p => p.TagID);

            modelBuilder.Entity<PortfolioImage>()
                .HasOne(pi => pi.Portfolio)
                .WithMany()
                .HasForeignKey(pi => pi.PortfolioID);

            modelBuilder.Entity<Comment>()
                .HasOne(c => c.Portfolio)
                .WithMany()
                .HasForeignKey(c => c.PortfolioID)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Comment>()
                .HasOne(c => c.Guest)
                .WithMany()
                .HasForeignKey(c => c.AccountID);
        }
    }
}
