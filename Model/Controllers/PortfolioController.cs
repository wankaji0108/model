﻿using Microsoft.AspNetCore.Mvc;
using Model.Models;
using Model.Repositories;
using System.Threading.Tasks;

namespace Model.Controllers
{
    public class PortfolioController : Controller
    {
        private readonly IPortfolioRepository _portfolioRepository;

        public PortfolioController(IPortfolioRepository portfolioRepository)
        {
            _portfolioRepository = portfolioRepository;
        }

        // Hiển thị danh sách portfolio
        public async Task<IActionResult> Index()
        {
            var portfolios = await _portfolioRepository.GetAllAsync();
            return View(portfolios);
        }

        // Hiển thị form thêm portfolio mới
        public IActionResult Add()
        {
            return View();
        }

        // Xử lý thêm portfolio mới
        [HttpPost]
        public async Task<IActionResult> Add(Portfolio portfolio)
        {
            if (ModelState.IsValid)
            {
                await _portfolioRepository.AddAsync(portfolio);
                return RedirectToAction(nameof(Index));
            }
            // Nếu ModelState không hợp lệ, hiển thị form với dữ liệu đã nhập
            return View(portfolio);
        }

        // Hiển thị thông tin chi tiết portfolio
        public async Task<IActionResult> Display(int id)
        {
            var portfolio = await _portfolioRepository.GetByIdAsync(id);
            if (portfolio == null)
            {
                return NotFound();
            }
            return View(portfolio);
        }

        // Hiển thị form cập nhật portfolio
        public async Task<IActionResult> Update(int id)
        {
            var portfolio = await _portfolioRepository.GetByIdAsync(id);
            if (portfolio == null)
            {
                return NotFound();
            }
            return View(portfolio);
        }

        // Xử lý cập nhật portfolio
        [HttpPost]
        public async Task<IActionResult> Update(int id, Portfolio portfolio)
        {
            if (id != portfolio.PortfolioId)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                await _portfolioRepository.UpdateAsync(portfolio);
                return RedirectToAction(nameof(Index));
            }
            return View(portfolio);
        }

        // Hiển thị form xác nhận xóa portfolio
        public async Task<IActionResult> Delete(int id)
        {
            var portfolio = await _portfolioRepository.GetByIdAsync(id);
            if (portfolio == null)
            {
                return NotFound();
            }
            return View(portfolio);
        }

        // Xử lý xóa portfolio
        [HttpPost, ActionName("Delete")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _portfolioRepository.DeleteAsync(id);
            return RedirectToAction(nameof(Index));
        }
    }
}
