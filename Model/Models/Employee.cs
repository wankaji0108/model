﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Model.Models
{
    public class Employee
    {
        [Key]
        public int EmployeeID { get; set; } 

        [ForeignKey("Account")]
        public int AccountID { get; set; }

        public Account Account { get; set; }
    }
}
