﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Model.Models
{
    public class User
    {
        [Key]
        [StringLength(50)]
        public string UserID { get; set; }


        public string Name { get; set; }
        public string Email { get; set; }

        [ForeignKey("Role")]
        public int RoleID { get; set; }

        public Role Role { get; set; }
    }
}
