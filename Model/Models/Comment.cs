﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Model.Models
{
    public class Comment
    {
        [Key]
        public int CommentID { get; set; }

        public int PortfolioID { get; set; }

        public int AccountID { get; set; }

        public string Content { get; set; }

        [ForeignKey("PortfolioID")]
        public Portfolio Portfolio { get; set; }

        [ForeignKey("AccountID")]
        public Guest Guest { get; set; }
    }
}
