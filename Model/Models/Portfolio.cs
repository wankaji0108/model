﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Model.Models
{

        public class Portfolio
        {
            [Key]
            public int PortfolioId { get; set; }

            public int AccountID { get; set; }

            [ForeignKey("AccountID")]
            public Guest Guest { get; set; }

            public string TagID { get; set; }

            [ForeignKey("TagID")]
            public Tag Tag { get; set; }
        }
}
