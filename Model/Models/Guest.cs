﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Model.Models
{
    public class Guest
    {
        [Key]
        [ForeignKey("Account")]
        public int AccountID { get; set; }

        [StringLength(55)]
        public string Company { get; set; }
        //
        public Account Account { get; set; }
    }
}
