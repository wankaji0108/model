﻿using System.ComponentModel.DataAnnotations;

namespace Model.Models
{
    public class Tag
    {
        [Key]
        [StringLength(55)]
        public string TagID { get; set; }

        [StringLength(55)]
        public string TagName { get; set; }

        [StringLength(300)]
        public string Description { get; set; }
    }
}

