﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Model.Models
{
    public class PortfolioImage
    {
        [Key]
        public int ImageID { get; set; }

        [StringLength(55)]
        public string ImageURL { get; set; }

        [ForeignKey("Portfolio")]
        public int PortfolioID { get; set; }

        public Portfolio Portfolio { get; set; }
    }
}