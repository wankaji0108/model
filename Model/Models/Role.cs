﻿using System.ComponentModel.DataAnnotations;

namespace Model.Models
{
    public class Role
    {
        [Key]
        public int RoleID { get; set; }

        [StringLength(55)]
        public string RoleName { get; set; }

        public int Authority { get; set; }
    }
}
