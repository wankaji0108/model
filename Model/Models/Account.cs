﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Model.Models
{
    public class Account
    {
        [Key]
        public int AccountID { get; set; }

        [StringLength(55)]
        public string AccountName { get; set; }

        [Column(TypeName = "varchar(MAX)")]
        public string Email { get; set; }

        [StringLength(55)]
        public string Password { get; set; }

        [ForeignKey("User")]
        [StringLength(50)] 
        public string UserID { get; set; }

        public User User { get; set; }
    }
}
